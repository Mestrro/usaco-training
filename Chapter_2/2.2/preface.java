import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import static java.lang.Math.abs;
/*
ID: marko.m3
LANG: JAVA
TASK: preface
*/
class preface {
	static int numOfI=0;
	static int numOfV=0;
	static int numOfX=0;
	static int numOfL=0;
	static int numOfC=0;
	static int numOfD=0;
	static int numOfM=0;
	static void calculateRoman(int num) {
		int temp = num%10;
		if (temp==0) {
			
		}else if (temp<4) {
			numOfI=numOfI+temp;
		} else if (temp<9) {
			numOfI =numOfI+ abs(temp-5);
			numOfV = numOfV+1;
		} else {
			numOfI=numOfI+abs(temp-10);
			numOfX=numOfX+1;
		}
		temp = (num%100)/10;
		if (temp==0) {
			
		}else if (temp<4) {
			numOfX=numOfX+temp;
		} else if (temp<9) {
			numOfX =numOfX+ abs(temp-5);
			numOfL = numOfL+1;
		} else {
			numOfX=numOfX+abs(temp-10);
			numOfC=numOfC+1;
		}
		temp =(num%1000)/100;
		if (temp==0) {
			
		}else if (temp<4) {
			numOfC=numOfC+temp;
		} else if (temp<9) {
			numOfC =numOfC+ abs(temp-5);
			numOfD = numOfD+1;
		} else {
			numOfC=numOfC+abs(temp-10);
			numOfM=numOfM+1;
		}
		numOfM=num/1000 + numOfM;
		return;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("preface.in"));
		int max = Integer.parseInt(file.readLine());
		file.close();
		for (int i=1; i<max+1; i++) {
			calculateRoman(i);
		}
		PrintWriter output= new PrintWriter(new BufferedWriter(new FileWriter("preface.out")));
		if (numOfI!=0) output.println("I "+ numOfI);
		if (numOfV!=0) output.println("V "+ numOfV);
		if (numOfX!=0) output.println("X "+ numOfX);
		if (numOfL!=0) output.println("L "+ numOfL);
		if (numOfC!=0) output.println("C "+ numOfC);
		if (numOfD!=0) output.println("D "+ numOfD);
		if (numOfM!=0) output.println("M "+numOfM);
		output.close();
	} 
}
