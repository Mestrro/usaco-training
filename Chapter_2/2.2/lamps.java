import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import static java.lang.Math.abs;
/*
ID: marko.m3
LANG: JAVA
TASK: lamps
*/
class lamps {
	static boolean[] on;
	static boolean[] off;
	static List<String> solutions= new ArrayList<String>();
	static Set<String> visited = new LinkedHashSet<String>();
	private static void solve(boolean[] current, int counter, String prev) {
		String currentString="";
		for (int i=0; i<current.length;i++) {
			if (current[i]) {
				currentString=currentString+"1";
			} else {
				currentString=currentString+"0";
			}
		}
		currentString = currentString+counter;
		if (visited.contains(currentString)) {
			return;
		} else {
			visited.add(currentString);
		}
		currentString=null;
		if (counter==0) {
			if (isLegit(current)) {
				String output = "";
				for (int i=0; i<current.length;i++) {
					if (current[i]) {
						output=output+"1";
					} else {
						output=output+"0";
					}
				}
				if (solutions.contains(output)) {
					return;
				}
				solutions.add(output);
			}
			return;
		}
		solve(button1(current), counter-1, "1");
		solve(button2(current), counter-1, "2");
		solve(button3(current), counter-1, "3");
		solve(button4(current), counter-1, "4");
		
	}
	static boolean isLegit(boolean[] checked) {
		boolean valid=true;
		for (int i=0; i<checked.length; i++) {
			
			if (on[i]) {
				if (!checked[i]) {
					valid=false;
				}
			} else if (off[i]) {
				if (checked[i]) {
					valid=false;
				}
			}
		}
		return valid;
	}
	static boolean[] button1 (boolean[] current) {
		boolean[] temp = Arrays.copyOf(current,current.length);
		for (int i=0; i<current.length;i++) {
			temp[i]= current[i] ^ true;
		}


		return temp;
	}
	static boolean[] button2 (boolean[] current) {
		boolean[] temp2 = Arrays.copyOf(current,current.length);
		for (int i=0; i<current.length; i=i+2) {
			temp2[i] = current[i] ^ true;
		}


		return temp2;
	}
	static boolean[] button3 (boolean[] current) {
		boolean[] temp3 = Arrays.copyOf(current,current.length);
		for (int i=1; i<current.length; i=i+2) {
			temp3[i]=current[i]^true;
		}

		return temp3;
	}
	static boolean[] button4 (boolean[] current) {

		boolean[] temp4 = Arrays.copyOf(current,current.length);
		for (int i=0; i<current.length; i=i+3) {
			temp4[i] = current[i]^true;
		}

		return temp4;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("lamps.in"));
		String line = file.readLine();

		int num = Integer.parseInt(line);
		line = file.readLine();

		int counter = Integer.parseInt(line);
		line = file.readLine();
		
		on = new boolean[num];
		off = new boolean[num];
		boolean[] lamps = new boolean[num];
		for (int i=0; i<lamps.length;i++) {
			lamps[i]=true;
		}
		StringTokenizer st = new StringTokenizer(line);
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals("-1")) {
				break;
			}
			on[Integer.parseInt(token)-1]=true;
		}
		line=file.readLine();

		StringTokenizer st2 = new StringTokenizer(line);
		while (st2.hasMoreTokens()) {
			String token = st2.nextToken();
			if (token.equals("-1")) {
				break;
			}
			off[Integer.parseInt(token)-1]=true;
		}
		file.close();
		solve(lamps,Math.min(counter, 4),"");
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("lamps.out")));
		if (solutions.size()==0) {
			output.println("IMPOSSIBLE");
		} else {
			Collections.sort(solutions);
			for (int i=0; i<solutions.size();i++) {
				output.println(solutions.get(i));
			}
		}
		output.close();
	}

}
