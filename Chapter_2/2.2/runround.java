import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import static java.lang.Math.abs;
/*
ID: marko.m3
LANG: JAVA
TASK: runround
*/
class runround {
	static List<Integer> getArray(int num){
		List<Integer> ary = new ArrayList<Integer>();
		while (num>0) {
			ary.add(num%10);
			num/=10;
		}
		Collections.reverse(ary);
		return ary;
	}
	static boolean areDistinct(List<Integer> arr) { 
	        Set<Integer> s = new LinkedHashSet<Integer>(arr); 
	        return (s.size() == arr.size()); 
	    } 
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("runround.in"));
		int num = Integer.parseInt(file.readLine());
		file.close();
		boolean flag=false;
		List<Integer> arrayNum;
		while (!flag) {
			num++;

			flag=true;
			arrayNum = getArray(num);
			if (!areDistinct(arrayNum)) {
				flag=false;
				continue;
			}
			boolean[] flags = new boolean[arrayNum.size()];
			
			int current = arrayNum.get(0);
			int currentIdx = 0;
			while(!flags[0]) {
				
				int temp=current;
				currentIdx = (currentIdx+current)%arrayNum.size();
				current = arrayNum.get(currentIdx);
				if (flags[currentIdx]) {
					break;
				}

				if (current==temp) {
					break;
				} else {
					flags[currentIdx]=true;
				}

			}
			for (int i=0; i<flags.length;i++) {
				if (!flags[i]) {
					flag=false;
				}
			}
			if (flag) {
				break;
			}
		}
		
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("runround.out")));
		output.println(num);
		output.close();
	}
}
