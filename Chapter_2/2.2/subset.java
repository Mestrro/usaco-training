import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import static java.lang.Math.abs;
/*
ID: marko.m3
LANG: JAVA
TASK: subset
*/
class subset { 
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("subset.in"));
		long num = Integer.parseInt(file.readLine());
		file.close();
		long sum=0;
		long counter=0;
		List<Long> listOfNum= new ArrayList<Long>();
		List<Long> tempListOfNum = new ArrayList<Long>();
		sum=(num*(num+1))/2;
		for (int i=0; i<sum+1;i++) {
			listOfNum.add((long) 0);
			tempListOfNum.add((long) 0);
		}
		listOfNum.set(0, (long) 1);
		tempListOfNum.set(0, (long) 1);
		if (sum%2 ==1) {
			counter=0;
		} else {
			sum=sum/2;
			int temp=0;
			for (int i=1; i<=num; i++) {
				for (int j=0; j<= temp; j++) {
					long newNum = (tempListOfNum.get(j)+listOfNum.get(i+j));
					listOfNum.set(i+j, newNum);
				}
				tempListOfNum.clear();
				tempListOfNum.addAll(listOfNum);
				temp=i*(i+1)/2;
			}
			counter = listOfNum.get((int) sum)/2;
		}
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("subset.out")));
		output.println(counter);
		output.close();
	}
}
