import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
/*
ID: marko.m3
LANG: JAVA
TASK: sort3
*/
class sort3 {
	static int swap(boolean[] oneWeHave, boolean[] needed, boolean[] needed2,boolean[] secondWeHave,int num) {
		List<Integer> newCoord = new ArrayList<Integer>();
		int winner=0;
		for (int i=0; i<num; i++) {
			if (!needed[i]&&oneWeHave[i]) {
				newCoord.add(i);
			}
		}
		for (int i=0; i< newCoord.size();i++) {
			if (needed2[newCoord.get(i)]&&!secondWeHave[newCoord.get(i)]) {
				winner=i;
			}
		}
		return newCoord.get(winner);
	}
	public static void main (String [] args) throws IOException {
		
		BufferedReader file = new BufferedReader(new FileReader("sort3.in"));
		int num = Integer.parseInt(file.readLine());
		boolean[] ones = new boolean[num];
		boolean[] twos = new boolean[num];
		boolean[] threes = new boolean[num];
		boolean[] neededOnes = new boolean[num];
		boolean[] neededTwos = new boolean[num];
		boolean[] neededThrees = new boolean[num];
		int numOfOnes = 0;
		int numOfTwos = 0;
		for (int i=0; i<num; i++) {
			int unsortedNum = Integer.parseInt(file.readLine());
			if (unsortedNum == 1) {
				ones[i] = true;
				numOfOnes++;
			} else if(unsortedNum ==2) {
				twos[i]=true;
				numOfTwos++;
			} else {
				threes[i]=true;
			}
		}
		for (int i=0; i<num; i++) {
			if (i<numOfOnes) {
				neededOnes[i]=true;
				continue;
			} 
			if (i>=numOfOnes&&i<numOfOnes+numOfTwos) {
				neededTwos[i]=true;
				continue;
			};
			if (i>=numOfOnes+numOfTwos) {
				neededThrees[i]=true;
			}
			
		}
		file.close();
		int newCoord =0;
		int counter=0;
		for (int i=0; i<num; i++) {
			if (i<numOfOnes&&!ones[i]) {
				if (twos[i]==true) {
					newCoord = swap(ones,neededOnes,neededTwos,twos,num);
					twos[newCoord]=true;
					twos[i]=false;
					ones[newCoord]=false;
					ones[i]=true;
					counter++;
					continue;
				} else {
					newCoord = swap(ones,neededOnes,neededThrees,threes,num);
					threes[newCoord]=true;
					threes[i]=false;
					ones[newCoord]=false;
					ones[i]=true;
					counter++;
					continue;
				}
			}
			if (i>=numOfOnes&&i<numOfOnes+numOfTwos&&!twos[i]) {
				if (ones[i]==true) {
					newCoord = swap(twos,neededTwos,neededOnes,ones,num);
					ones[newCoord]=true;
					ones[i]=false;
					twos[newCoord]=false;
					twos[i]=true;
					counter++;
					continue;
				} else {
					newCoord = swap(twos,neededTwos,neededThrees,threes,num);
					threes[newCoord]=true;
					threes[i]=false;
					twos[newCoord]=false;
					twos[i]=true;
					counter++;
					continue;
				}
			}
			if (i>=numOfOnes+numOfTwos&&!threes[i]) {
				if (twos[i]==true) {
					newCoord = swap(threes,neededThrees,neededTwos,twos,num);
					twos[newCoord]=true;
					twos[i]=false;
					threes[newCoord]=false;
					threes[i]=true;
					counter++;
					continue;
				} else {
					newCoord = swap(threes,neededThrees,neededOnes,ones,num);
					ones[newCoord]=true;
					ones[i]=false;
					threes[newCoord]=false;
					threes[i]=true;
					counter++;
					continue;
				}
			}
		}

		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("sort3.out")));
		output.println(counter);
		output.close();
	}
}
