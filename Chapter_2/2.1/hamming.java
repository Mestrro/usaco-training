import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
/*
ID: marko.m3
LANG: JAVA
TASK: hamming
*/
class hamming {
	private static int calcDist(boolean[] first, boolean[] second) {
		int distance=0;
		for (int i=0; i<first.length;i++) {
			if (first[i]!=second[i]) {
				distance++;
			}
		}
		return distance;
	}
	private static int calculateDec(boolean[] bool) {
		int dec = 0;
		int bin = 1;
		for (int i=0; i<bool.length;i++) {
			if (bool[i]) {
				dec+=bin;
			}
			bin = bin*2;
		}
		return dec;
	}
	private static boolean[] calculateBin(int dec, int digits) {
		boolean[] bin = new boolean[digits];
		int num = dec;
		for (int i=0; i<digits; i++) {
			if (num%2 == 1) {
				bin[i]=true;
			}
			num=num/2;
		}
		return bin;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("hamming.in"));
		String line = file.readLine();
		StringTokenizer st = new StringTokenizer(line);
		int N = Integer.parseInt(st.nextToken());
		int B = Integer.parseInt(st.nextToken());
		int D = Integer.parseInt(st.nextToken());
		file.close();
		List<boolean[]> listOfNums = new ArrayList<boolean[]>();
		int counter = 0;
		int limit=1;
		boolean[] first = calculateBin(0,B);
		listOfNums.add(first);
		counter++;
		for (int i=0; i<B;i++) {
			limit = limit*2;
		}
		for (int i=1; i< limit; i++) {
			boolean[] second = new boolean[B];
			int counter2=0;
			second = calculateBin(i, B);
			for (int j=0; j<listOfNums.size();j++) {
				first = listOfNums.get(j);
				if (calcDist(first,second)>=D) {
					counter2++;
				}
			}
			if (counter2==listOfNums.size()) {
				listOfNums.add(second);
				counter++;
			}
			if (counter==N) {
				break;
			}
		}
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("hamming.out")));
		for (int i=1; i<listOfNums.size()+1; i++) {
			if (i==listOfNums.size()) {
				outputFile.println(calculateDec(listOfNums.get(i-1)));
				break;
			}
			if (!(i%10==0)){
				outputFile.print(calculateDec(listOfNums.get(i-1))+" ");
			}else {
				outputFile.print(calculateDec(listOfNums.get(i-1)));
				outputFile.println();
			}
		}
		outputFile.close();
	}
}
