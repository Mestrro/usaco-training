import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
/*
ID: marko.m3
LANG: JAVA
TASK: frac1
*/
class frac1 {
	static int gcd(int a, int b) {
		int gcd=0;
		for(int i = 1; i <= a && i <= b; i++)
        {
            if(a%i==0 && b%i==0)
                gcd = i;
        } 
		return gcd;
    } 
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("frac1.in"));
		int max = Integer.parseInt(file.readLine());
		file.close();
		double current=0;
		List<String> listOfNums = new ArrayList<>();
		listOfNums.add("0 0/1");
		for (int deNum = 0; deNum < max+1; deNum++) {
			for (int num = 0; num< deNum+1; num++ ) {
				if (gcd(num,deNum)==1) {
					double newNum = num;
					current = newNum/deNum;
					listOfNums.add(current+" "+num+"/"+deNum);
				}
			}
		}
		Collections.sort(listOfNums);
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("frac1.out")));
		for (int i=0; i<listOfNums.size();i++) {
			String currentLine = listOfNums.get(i);
			StringTokenizer st = new StringTokenizer(currentLine);
			st.nextToken();
			output.println(st.nextToken());
		}
		output.close();
	}
}
