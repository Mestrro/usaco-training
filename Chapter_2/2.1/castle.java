import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.math.*;
/*
ID: marko.m3
LANG: JAVA
TASK: castle
*/
class castle {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("castle.in"));
		String line = file.readLine();
		StringTokenizer st = new StringTokenizer(line);
		int vertical = Integer.parseInt(st.nextToken());
		int horizontal = Integer.parseInt(st.nextToken());
		String[][] castle = new String[horizontal][vertical];
		
		Map<String, String> walls  = new HashMap<String, String>() {{
			put("0", "O");
		    put("1", "W");
		    put("2", "N");
		    put("3", "WN");
		    put("4", "E");
		    put("5", "WE");
		    put("6", "NE");
		    put("7", "WNE");
		    put("8", "S");
		    put("9", "WS");
		    put("10", "NS");
		    put("11", "WNS");
		    put("12", "ES");
		    put("13", "WES");
		    put("14", "NES");
		    put("15", "WNES");
		}};
		
		for (int i = 0; i < horizontal; i++) {
			line = file.readLine();
			StringTokenizer st2 = new StringTokenizer(line);
			for (int j=0; j< vertical; j++) {
				castle[i][j] = walls.get(st2.nextToken());
			}
			
		}
		int mark = 1;
		List<String> list = new ArrayList<>();
		int[][] rooms = new int[horizontal][vertical];
		for (int i = 0; i< horizontal; i++) {
			for (int j = 0; j< vertical; j++) {
				if (rooms[i][j]==0) {
					rooms[i][j] = mark;
					mark++;
				}
				if (i +1< horizontal) {
					if (!castle[i+1][j].contains("N")) {
						if (rooms[i+1][j]>0&&rooms[i][j]!=rooms[i+1][j]) {
							if (!list.contains(Math.min(rooms[i+1][j], rooms[i][j])+" "+Math.max(rooms[i+1][j], rooms[i][j]))) {
								list.add(" "+Math.min(rooms[i+1][j], rooms[i][j])+" "+Math.max(rooms[i+1][j], rooms[i][j]));
							}
						}
						rooms[i+1][j] = rooms[i][j];
					}
				}

				if (j +1< vertical) {
					if (!castle[i][j+1].contains("W")) {
						if (rooms[i][j+1]>0&&rooms[i][j]!=rooms[i][j+1]) {
							if (!list.contains(Math.min(rooms[i][j+1], rooms[i][j])+" "+Math.max(rooms[i][j+1], rooms[i][j]))) {
								list.add(" "+Math.min(rooms[i][j+1], rooms[i][j])+" "+Math.max(rooms[i][j+1], rooms[i][j]));
							}
						}
						rooms[i][j+1] = rooms[i][j];
					}
				}
				if (i -1>=0) {
					if (!castle[i-1][j].contains("S")) {
						if (rooms[i-1][j]>0&&rooms[i][j]!=rooms[i-1][j]) {
							if (!list.contains(Math.min(rooms[i-1][j], rooms[i][j])+" "+Math.max(rooms[i-1][j], rooms[i][j]))) {
								list.add(" "+Math.min(rooms[i-1][j], rooms[i][j])+" "+Math.max(rooms[i-1][j], rooms[i][j]));
							}
						}
						rooms[i-1][j] = rooms[i][j];
					}
				}
				if (j -1>= 0) {
					if (!castle[i][j-1].contains("E")) {
						if (rooms[i][j-1]>0&&rooms[i][j]!=rooms[i][j-1]) {
							if (!list.contains(Math.min(rooms[i][j-1], rooms[i][j])+" "+Math.max(rooms[i][j-1], rooms[i][j]))) {
								list.add(" "+Math.min(rooms[i][j-1], rooms[i][j])+" "+Math.max(rooms[i][j-1], rooms[i][j]));
							}
						}
						rooms[i][j-1] = rooms[i][j];
					}
				}
			}
		}
		file.close();
		boolean zastava=true;
		while (zastava) {
			zastava=false;
			for (int i=0; i < list.size(); i++) {
				for (int j=i+1; j<list.size()-1;j++)  {
					StringTokenizer tokenizer = new StringTokenizer(list.get(i));
					while (tokenizer.hasMoreTokens()) {
							String first = tokenizer.nextToken();
							if (list.get(j).contains(" "+first+" ")) {
								list.set(i,list.get(i)+" "+list.get(j)+" ");
								list.remove(j);
								j--;
								zastava=true;
								break;
							}
					}
				}
			}
		}
		for (int i=0; i<list.size();i++) {
			System.out.println(list.get(i));
			System.out.println("____________________");
		}
		for (int i=0; i< horizontal; i++) {
			for (int j=0; j<vertical; j++) {
				for (int k=0; k<list.size();k++)  {
					if (list.get(k).contains(" "+rooms[i][j]+" ")) {
						rooms[i][j]=mark+k;
					}
				}
			}
		}
		Map<Integer, Integer> roomSizes  = new LinkedHashMap<Integer, Integer>();
		for (int i=0; i< horizontal; i++) {
			for (int j=0; j<vertical; j++) {
				if (roomSizes.containsKey(rooms[i][j])) {
					roomSizes.put(rooms[i][j], roomSizes.get(rooms[i][j])+1);
				} else {
					roomSizes.put(rooms[i][j], 1);
				}
			}
		}
		int max = 0; 
		for (Map.Entry<Integer,Integer> entry : roomSizes.entrySet())  {
			if (entry.getValue()>max) {
				max = entry.getValue();
			}
		}
		int newMax = 0;
		int firstCoord=0;
		int secondCoord=0;
		String letter = "";
		for (int j=0; j<vertical; j++) {
		
			for (int i=horizontal-1; i>=0; i--) {
				int here = rooms[i][j];
				if (i-1>=0) {
					int up = rooms[i-1][j];
					if (here!=up) {
						if (roomSizes.get(here)+roomSizes.get(up)>newMax) {
							newMax = roomSizes.get(here)+roomSizes.get(up);
							firstCoord = i+1;
							secondCoord = j+1;
							letter = "N";
						}
					}
				}
				if (j+1<vertical) {
					int right = rooms[i][j+1];
					if (here!=right) {
						if (roomSizes.get(here)+roomSizes.get(right)>newMax) {
							newMax = roomSizes.get(here)+roomSizes.get(right);
							firstCoord = i+1;
							secondCoord = j+1;
							letter = "E";
						}
					}
				}
			}
		}
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("castle.out")));
		output.println(roomSizes.size());
		output.println(max);
		output.println(newMax);
		output.println(firstCoord+" "+secondCoord+" "+letter);
		output.close();
	}
}
