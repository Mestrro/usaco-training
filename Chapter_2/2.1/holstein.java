import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
/*
ID: marko.m3
LANG: JAVA
TASK: holstein
*/
class holstein {
	static int curMin=16;
	static Set<String> allSolutions2 = new LinkedHashSet<String>();
	static Integer solutions=0;
	static List<Integer> solutionsScoop = new ArrayList<Integer>();
	private static void recur(int[] cur, List<int[]> scoops, List<Integer> curScoops) {
		if (curScoops.size()>=curMin) {
			return;
		}
		
		if (finished(cur)) {
			curMin=curScoops.size();
			solutions=curScoops.size();
			solutionsScoop.clear();

			solutionsScoop.addAll(curScoops);
			return;
		}
		
		int[] tempInt;
		
		Collections.sort(curScoops);
		if (allSolutions2.contains(curScoops.toString())) {
			return;
		}
		allSolutions2.add(curScoops.toString());
		List<Integer> temp2 = new ArrayList<Integer>();
		int starter=0;
		if (curScoops.size()>0) {
			starter = curScoops.get(curScoops.size()-1);
		}
		for (int i=starter; i<scoops.size();i++) {
			if (curScoops.size()>=curMin) {
				return;
			}

			temp2.addAll(curScoops);
			temp2.add(scoops.get(i)[0]);
			tempInt = takeAScoop(cur, scoops.get(i)); 
			if (!curScoops.contains(scoops.get(i)[0])) {
				recur(tempInt,scoops,temp2);
			} else {
				continue;
			}

			Collections.sort(temp2);
			allSolutions2.add(temp2.toString());
			temp2.clear();

			
		}
	}
	static int[] takeAScoop(int[] cur, int[] scoop) {
		int[] output = new int[cur.length];
		for (int i=0; i<cur.length;i++) {
			output[i]=cur[i]-scoop[i+1];
		}
		return output;
	}
	static boolean finished(int[] cur) {
		boolean finished = true;
		for (int i=0; i<cur.length;i++ ) {
			if (cur[i]>0) {
				finished=false;
			}
		}
		return finished;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("holstein.in"));
		int numOfVit= Integer.parseInt(file.readLine());
		int[] cur = new int[numOfVit];
		String line = file.readLine();
		cur[0]=0;
		StringTokenizer st = new StringTokenizer(line);
		for (int i=0; i<numOfVit; i++) {
			
			cur[i]=Integer.parseInt(st.nextToken());
		}
		line = file.readLine();
		int numOfScoop = Integer.parseInt(line);
		List<int[]> scoops = new ArrayList<int[]>();
		for (int i=0; i<numOfScoop; i++) {
			scoops.add(new int[numOfVit+1]);
			line=file.readLine();
			StringTokenizer st2 = new StringTokenizer(line);
			scoops.get(i)[0]=i+1;
			for (int j=1; j<numOfVit+1; j++) {
				scoops.get(i)[j] = Integer.parseInt(st2.nextToken());
			}
		}
		file.close();
		List<Integer> curSol = new ArrayList<Integer>();
		recur(cur,scoops,curSol);
		String solution;
		solution = solutions+" ";
		for (int i=0; i<solutions; i++) {
			if (i==solutions-1) {
				solution = solution+solutionsScoop.get(i);
				break;
			}
			solution= solution+solutionsScoop.get(i)+" ";
		}
		PrintWriter output2 = new PrintWriter(new BufferedWriter(new FileWriter("holstein.out")));
		output2.println(solution);
		output2.close();
	}
}
