/*
ID: marko.m3
LANG: JAVA
TASK: ride
*/
import java.io.*;

class ride {
	 public static void main (String [] args) throws IOException {
		 BufferedReader planetAndComet = new BufferedReader(new FileReader("ride.in"));
		 String line;
		 String outputWord;
		 int[] arrayOfModLines = {1,1};
		 for (int i=0; i<2; i++) {
			 line = planetAndComet.readLine();
			 for (int j=0; j<line.length();j++) {
				int valueOfLetter = (int) line.charAt(j) - 64;
				arrayOfModLines[i] = arrayOfModLines[i] * valueOfLetter;
			 }
			 arrayOfModLines[i] = arrayOfModLines[i] % 47;
		 }
		 planetAndComet.close();
		 if (arrayOfModLines[0] == arrayOfModLines[1]) {
			 outputWord = "GO";
		 } else 
		 {
			 outputWord = "STAY";
		 }
		 PrintWriter outputOrder = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
		 outputOrder.println(outputWord);
		 outputOrder.close();
	 }
}
