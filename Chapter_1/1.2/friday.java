/*
ID: marko.m3
LANG: JAVA
TASK: friday
*/
import java.io.*;
import java.util.*;
class friday {
	public static void main (String [] args) throws IOException {
		int first = 0;
		int days = 0;
		int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30};
		int[] numOfDays = {0, 0, 0, 0, 0, 0, 0};
		BufferedReader yearsFile = new BufferedReader(new FileReader("friday.in"));
		String line = yearsFile.readLine();
		int sumOfDays = 12;
		int day = 0;
		int N = Integer.parseInt(line);
		for (int i=0; i<N; i++) {
			days = 365;
			months[1] = 28;
			if (((i+1900) % 4 == 0 && (i+1900)%100 != 0) || ((i + 1900) % 400 == 0)) {
					days = 366;
					months[1] = 29;
			}
			day = (sumOfDays + first) % 7;
			numOfDays[day] = numOfDays[day]+1;

			for (int j=0; j<11; j++) {
				sumOfDays = months[j] + sumOfDays;
				day = (sumOfDays + first) % 7;
				numOfDays[day] = numOfDays[day]+1;
			}
			first = (first + days) % 7;
			sumOfDays = 12;
		}
		yearsFile.close();
		PrintWriter outputDays = new PrintWriter(new BufferedWriter(new FileWriter("friday.out")));
		outputDays.println(numOfDays[5]+" "+numOfDays[6]+" "+numOfDays[0]+" "+numOfDays[1]+" "+numOfDays[2]+" "+numOfDays[3]+" "+numOfDays[4]);
		outputDays.close();
	}

}
