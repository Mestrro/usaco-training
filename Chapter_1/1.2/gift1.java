/*
ID: marko.m3
LANG: JAVA
TASK: gift1
*/
import java.io.*;
import java.util.*;
class gift1 {
	public static void main (String [] args) throws IOException {
		 BufferedReader peopleAndGifts = new BufferedReader(new FileReader("gift1.in"));
		 String line = peopleAndGifts.readLine();
		 int numberOfPeople = Integer.parseInt(line);
		 LinkedHashMap<String, Integer> namesAndValues = new LinkedHashMap<String, Integer>();

		 for (int j = 0; j < numberOfPeople; j++) {
			 	
			 	line = peopleAndGifts.readLine();
			 	namesAndValues.put(line, 0);
				 
		 }
		 while (line != null) {
			 line = peopleAndGifts.readLine();
			 String givingPerson = line;

			 line = peopleAndGifts.readLine();
			 if (line == null) {
				 break;
			 }
			 StringTokenizer st = new StringTokenizer(line);
			 int givenMoney = Integer.parseInt(st.nextToken()); 
			 int numGiftRecv = Integer.parseInt(st.nextToken()); 
			 if (numGiftRecv == 0) {

				 continue;
			 }

			 int currentMoney = (int)namesAndValues.get(givingPerson);
			 int money = (givenMoney % numGiftRecv) - givenMoney + (int) namesAndValues.get(givingPerson);
			 namesAndValues.put(givingPerson, money);
			 for (int i = 0; i < numGiftRecv; i++) {
				 line = peopleAndGifts.readLine();
				 System.out.println(givingPerson+" giving "+line+" this much money "+givenMoney/numGiftRecv);
				 int newValue = (int) namesAndValues.get(line)+givenMoney/numGiftRecv;
				 namesAndValues.put(line, newValue);
			 }

			 
		 }
		 peopleAndGifts.close();
		 PrintWriter outputPeopleAndMoney = new PrintWriter(new BufferedWriter(new FileWriter("gift1.out")));
		 for (Map.Entry<String, Integer> entry : namesAndValues.entrySet()) {
			    Integer integer = entry.getValue();
			    String name = entry.getKey();
			    outputPeopleAndMoney.println(name+" "+integer);
			    }
		 
		 outputPeopleAndMoney.close();
	}
}
