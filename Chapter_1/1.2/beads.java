/*
ID: marko.m3
LANG: JAVA
TASK: beads
*/
import java.io.*;
import java.util.*;

class beads {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("beads.in"));
		String line = file.readLine();
		int numOfBeads = Integer.parseInt(line);
		String beads = file.readLine();
		beads = beads + beads;
		System.out.println(beads);
		file.close();
		int maxSum = 0;
		int forward = 1;
		int backward = 1;
		int forwardCounter = 0;
		int backwardCounter = 0;
		int sum = 0;
		String forwardString = "";
		String backwardString = "";
		for (int i = 0; i < numOfBeads; i++) {
			forwardCounter = i;
			backwardCounter = i + numOfBeads-1;
			forwardString =""+ beads.charAt(forwardCounter);
			backwardString =""+ beads.charAt(backwardCounter);
			while ((beads.charAt(forwardCounter) == beads.charAt(forwardCounter+1)||(beads.charAt(forwardCounter+1)== 'w')||(beads.charAt(forwardCounter)== 'w'))) {
				forward = forward + 1;
				forwardCounter = forwardCounter +1;
				forwardString = forwardString + beads.charAt(forwardCounter);
				if (forwardString.contains("r") && forwardString.contains("b")){
					forwardString = forwardString.substring(0, forwardString.length() - 1);
					forward = forward - 1;
					break;
				}
				if (forwardString.length() == numOfBeads) {
					break;
				}
				
			}

			
			while ((beads.charAt(backwardCounter) == beads.charAt(backwardCounter-1)||(beads.charAt(backwardCounter-1)== 'w')||(beads.charAt(backwardCounter)== 'w'))) {
				backward = backward + 1;
				backwardCounter = backwardCounter - 1;
				backwardString = backwardString + beads.charAt(backwardCounter);
				if (backwardString.contains("r") && backwardString.contains("b")){
					backwardString = backwardString.substring(0, backwardString.length() - 1);
					backward = backward - 1;
					break;
				}
				if (backwardCounter == forwardCounter) {
					break;
				}
				if (backwardString.length() == numOfBeads) {
					break;
				}
			}
			
			sum = forward + backward;
			if (sum > maxSum) {
				String bestforward = forwardString;
				String bestbackward = backwardString;
				maxSum = sum;
			}
			forward = 1;
			backward = 1;

		}
		if (maxSum == 2 * numOfBeads) {
			maxSum = numOfBeads;
		}
		PrintWriter outputDays = new PrintWriter(new BufferedWriter(new FileWriter("beads.out")));
		outputDays.println(maxSum);
		outputDays.close();
	}
}