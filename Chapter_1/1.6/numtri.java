import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: marko.m3
LANG: JAVA
TASK: numtri
*/
class numtri {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("numtri.in"));
		String line = file.readLine();
		int first=0;
		int second = 0;
		int rows = Integer.parseInt(line);
		int[][] triangle = new int[rows][rows];
		for (int i=0; i<rows; i++) {
			line = file.readLine();
			if (i==0) {
				triangle[0][0]=Integer.parseInt(line);
				System.out.println(triangle[0][0]);
				continue;
			}
			StringTokenizer st = new StringTokenizer(line);
			for (int j=0; j<=i; j++) {
				int a = Integer.parseInt(st.nextToken());
				first = triangle[i-1][j]+a;
				if (j==0) {
					second = 0;
				} else {
					second = triangle[i-1][j-1]+a;
				}
				triangle[i][j] = Math.max(first, second);
			}
		}
		file.close();
		int max=0;
		for (int k=0; k<rows; k++) {
			if (triangle[rows-1][k]>max) {
				max = triangle[rows-1][k];
			}
		}
		PrintWriter outputNum = new PrintWriter(new BufferedWriter(new FileWriter("numtri.out")));
		outputNum.println(max);
		outputNum.close();
	}
}
