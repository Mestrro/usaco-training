import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/*
ID: marko.m3
LANG: JAVA
TASK: sprime
*/
class sprime {
	static boolean isPrime(int n) {
		if (n==1||n==0) {
			return false;
		}
		for (int i=2; i<=Math.sqrt(n);i++) {
			if (n%i == 0) {
				return false;
			}
		}
        return true;
    } 
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("sprime.in"));
		String line = file.readLine();
		int digits = Integer.parseInt(line);
		file.close();
		List<Integer> sprimes = new ArrayList<Integer>();
		for (int i=0;i<digits;i++) {
			if (sprimes.isEmpty()) {
				for (int j=0; j<10;j++) {
					if (isPrime(j)) {
						sprimes.add(j);
					}
				}
				continue;
			}
			List<Integer> alternate = new ArrayList<Integer>();
			for (int k=0; k<sprimes.size(); k++) {
				for (int l=0; l<10; l++) {
					if (isPrime(sprimes.get(k)*10+l)) {
						alternate.add(sprimes.get(k)*10+l);
					}
				}
			}
			sprimes=alternate;
		}
		Collections.sort(sprimes);
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("sprime.out")));
		for (int l=0; l<sprimes.size(); l++) {
			output.println(sprimes.get(l));
		}
		output.close();
	}

}
