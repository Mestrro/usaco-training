import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

import java.util.*;

/*
ID: marko.m3
LANG: JAVA
TASK: pprime
*/
class pprime {
	static boolean isPrime(int n) {
		for (int i=2; i<=Math.sqrt(n);i++) {
			if (n%i == 0) {
				return false;
			}
		}
        return true;
    } 
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("pprime.in"));
		String line = file.readLine();
		StringTokenizer st = new StringTokenizer(line);
		List<Integer> palindromes = new ArrayList<Integer>();
		int bottom = Integer.parseInt(st.nextToken());
		String topNum = st.nextToken();
		int top = Integer.parseInt(topNum);
		file.close();
		int digits = topNum.length();
		for (int i=0; i<10;i++) {
			if (isPrime(i)&&i<=top&&i>=bottom) {
				palindromes.add(i);
			}
			if (isPrime(10*i+i)&&10*i+i<=top&&10*i+i>=bottom) {
				palindromes.add(10*i+i);
			}
			if (digits>2) {
			for (int j=0; j<10;j++) {
				if (isPrime(i*100+10*j+i)&&i*100+10*j+i<=top&&i*100+10*j+i>=bottom) {
					palindromes.add(i*100+10*j+i);
				}
				if (isPrime(i*1000+j*100+10*j+i)&&i*1000+j*100+10*j+i<=top&&i*1000+j*100+10*j+i>=bottom) {
					palindromes.add(i*1000+j*100+10*j+i);
				}
				if (digits>4) {
					for (int k=0; k<10;k++) {
						if (isPrime(i*10000+j*1000+100*k+10*j+i)&&i*10000+j*1000+100*k+10*j+i<=top&&i*10000+j*1000+100*k+10*j+i>=bottom) {
							palindromes.add(i*10000+j*1000+100*k+10*j+i);
						}
						if (isPrime(i*100000+j*10000+1000*k+100*k+10*j+i)&&i*100000+j*10000+1000*k+100*k+10*j+i<=top&&i*100000+j*10000+1000*k+100*k+10*j+i>=bottom) {
							palindromes.add(i*100000+j*10000+1000*k+100*k+10*j+i);
						}
						if (digits>6) {
							for (int l=0; l<10;l++) {
								if (isPrime(i*1000000+j*100000+10000*k+1000*l+100*k+10*j+i)&&i*1000000+j*100000+10000*k+1000*l+100*k+10*j+i<=top&&i*1000000+j*100000+10000*k+1000*l+100*k+10*j+i>=bottom) {
									palindromes.add(i*1000000+j*100000+10000*k+1000*l+100*k+10*j+i);
								}
								if (isPrime(i*10000000+j*1000000+100000*k+10000*l+1000*l+100*k+10*j+i)&&i*10000000+j*1000000+100000*k+10000*l+1000*l+100*k+10*j+i<=top&&i*10000000+j*1000000+100000*k+10000*l+1000*l+100*k+10*j+i>=bottom) {
									palindromes.add(i*10000000+j*1000000+100000*k+10000*l+1000*l+100*k+10*j+i);
								}
								}
							}
						}
					}
				}
			}
		}
		Collections.sort(palindromes);
		PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("pprime.out")));
		for (int i=0; i<palindromes.size();i++) {
			output.println(palindromes.get(i));
		}
		output.close();
	}
}
