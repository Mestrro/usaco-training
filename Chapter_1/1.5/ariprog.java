/*
ID: marko.m3
LANG: JAVA
TASK: ariprog
*/

import java.io.*;
import java.util.*;
class ariprog {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("ariprog.in"));
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("ariprog.out")));
		String line = file.readLine();
		int N = Integer.parseInt(line);
		line = file.readLine();
		int M = Integer.parseInt(line);
		boolean [] contained = new boolean[M*M+M*M+1];
		file.close();
		Set<Integer> S = new LinkedHashSet<Integer>();
		for (int i=0; i <= M; i++) {
			for (int j=0; j<=M;j++) {
				S.add(i*i+j*j);
				contained[i*i+j*j]=true;
			}
		}
		List<Integer> SList = new ArrayList<Integer>(); 
	    for (Integer x : S) {
	    	SList.add(x); 

	    }
	    Collections.sort(SList);
		int a;
		int b;
		int counter;
		ArrayList<String> needToSort = new ArrayList<String>();
		for (int j=0; j<SList.size();j++) {
			for (int k=j+1; k<SList.size();k++) {
				a=SList.get(j);
				b=SList.get(k)-a;
				counter=0;
				if (a+(N-1)*b>contained.length-1) {
					break;
				}
				for (int l=0; l<N;l++) {
					if (contained[a+l*b]) {
						counter++;
					} else {
						break;
					}
				}
				
				if (counter==N) {
					if (needToSort.size()==0) {
						needToSort.add(a+" "+b);
						continue;
					}
					for (int l=0;l<needToSort.size();l++) {
						StringTokenizer st = new StringTokenizer(needToSort.get(l));
						int a2 = Integer.parseInt(st.nextToken());
						int b2 = Integer.parseInt(st.nextToken());
						if (b< b2) {
								needToSort.add(l, a+" "+b);
								break;
						}
						if (b == b2) {
							if (a<a2) {
								needToSort.add(l,a+" "+b);
								break;
							}
						}
						
					}
					if (!needToSort.contains(a+" "+b)) {
						needToSort.add(a+" "+b);
					}

					}
				
			}
		}
		for (int i=0; i<needToSort.size(); i++) {
			outputFile.println(needToSort.get(i));
		}
		if (needToSort.size()==0) {
			outputFile.println("NONE");
		}
		outputFile.close();
	}

}
