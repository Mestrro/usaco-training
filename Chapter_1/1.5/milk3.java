/*
ID: marko.m3
LANG: JAVA
TASK: milk3
*/

import java.io.*;
import java.util.*;
class milk3 {
	static boolean [][][] states;
	static Set<Integer> solutions = new LinkedHashSet<Integer>();
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("milk3.in"));
		String line = file.readLine();
		file.close();
		StringTokenizer st = new StringTokenizer(line);
		int[] max = new int[3];
		for (int i=0; i<3; i++) {
			max[i]=Integer.parseInt(st.nextToken());
		}
		states = new boolean[21][21][21];
		int[] milk = new int[3];
		milk[2]=max[2];
		pouring(max, milk);
		PrintWriter outputMilk = new PrintWriter(new BufferedWriter(new FileWriter("milk3.out")));
		String output = "";
		List<Integer> sortedList = new ArrayList<>(solutions);
		Collections.sort(sortedList);
		for (int i=0; i<solutions.size(); i++) {
			if (i==0) {
				output += sortedList.get(0);
				continue;
			}
			output += " "+sortedList.get(i);
		}
		outputMilk.println(output);
		outputMilk.close();
	}
	static void pouring(int[] max, int[] milk){

		if (states[milk[0]][milk[1]][milk[2]]) {
			return;
		}
		if (milk[0]==0) {
			solutions.add(milk[2]);
		}
		states[milk[0]][milk[1]][milk[2]]=true;
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				int[] old =Arrays.copyOf(milk,milk.length);
				if (i==j||milk[i]==0||milk[j]==max[j]) {
					continue;
				}
				if (milk[i]+milk[j]>max[j]) {
					milk[i]=milk[i]-max[j]+milk[j];
					milk[j] = max[j];
				} else {
					milk[j] = milk[i]+milk[j];
					milk[i] = 0;
				}
				if (states[milk[0]][milk[1]][milk[2]]) {
					milk=old;
					continue;
				}
				pouring(max, milk);
			}
		}
	}
	
}
