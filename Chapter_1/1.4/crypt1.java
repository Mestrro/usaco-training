/*
ID: marko.m3
LANG: JAVA
TASK: crypt1
*/
import java.io.*;
import java.util.*;
class crypt1 {
	static Boolean isValid(String number, String[] digits) {
		Boolean valid=false;
		int counter=0;
		for (int i=0; i< number.length();i++) {
			for (int j=0; j< digits.length; j++) {
				if (Character.toString(number.charAt(i)).equals(digits[j])) {
					counter++;
					break;
				} 
			}			
		}
		if (counter == number.length()) {
			valid =true;
		}
		return valid;	
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("crypt1.in"));
		String line = file.readLine();
		int numOfDigits = Integer.parseInt(line);
		line = file.readLine();

		String[] digits = new String[numOfDigits];
		StringTokenizer st = new StringTokenizer(line);
		int i=0;
		while (st.hasMoreTokens()) {
			digits[i] =st.nextToken();
			i++;
		}
		file.close();
		int counter=0;
		int firstNum;
		int secondNum;
		int firstPart;
		int secondPart;
		int finalized;
		for (int j=0; j< numOfDigits; j++) {
			for (int k=0; k< numOfDigits; k++) {
				for (int l=0; l< numOfDigits; l++) {
					for (int m=0; m< numOfDigits; m++) {
						for (int n=0; n< numOfDigits; n++) {
							firstNum = 100 * Integer.parseInt(digits[j])+ 10* Integer.parseInt(digits[k])+Integer.parseInt(digits[l]);
							secondNum = 10 * Integer.parseInt(digits[m])+ Integer.parseInt(digits[n]);
							firstPart = (secondNum/10)* firstNum;
							secondPart = (secondNum%10) * firstNum;
							finalized = firstNum * secondNum;
							if (isValid(Integer.toString(firstPart),digits) && isValid(Integer.toString(secondPart),digits) &&isValid(Integer.toString(finalized),digits)&&!(finalized/10>1000)&&!(firstPart/10>100)&&!(secondPart/10>100)) {
								counter ++;
								System.out.println("Novi racun");
								System.out.println(firstNum);
								System.out.println(secondNum);
								System.out.println("_______________");
								System.out.println(firstPart);
								System.out.println(secondPart);
								System.out.println("_______________");
								System.out.println(finalized);
								System.out.println("_______________");
								System.out.println("brojac "+counter);
								System.out.println("________________");
							}
						}
					}
				}
			}
		}
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("crypt1.out")));
		outputFile.println(counter);
		outputFile.close();
		
	}
}
