/*
ID: marko.m3
LANG: JAVA
TASK: wormhole
*/

import java.io.*;
import java.util.*;
class wormhole {
    private static void compute(Set<Integer> set,
            List<List<Integer>> currentResults,
            List<List<List<Integer>>> results)
        {
            if (set.size() < 2)
            {
                results.add(new ArrayList<List<Integer>>(currentResults));
                return;
            }
            List<Integer> list = new ArrayList<Integer>(set);
            Integer first = list.remove(0);
            for (int i=0; i<list.size(); i++)
            {
                Integer second = list.get(i);
                Set<Integer> nextSet = new LinkedHashSet<Integer>(list);
                nextSet.remove(second);

                List<Integer> pair = Arrays.asList(first, second);
                currentResults.add(pair);
                compute(nextSet, currentResults, results);
                currentResults.remove(pair);
            }
        }
    private static boolean isNextTo(int x1, int y1, int x2, int y2, int[] xArray, int[] yArray) {
    	if (y1==y2) {
    		if (x2>x1) {

    		for (int i=0; i<xArray.length; i++) {
    			if (yArray[i]==y1) {
    				if (xArray[i]<x2 && xArray[i]> x1) {
    					return false;
    				}
    			}
    		}
    		return true;
    		}
    		
    	}
    	return false;
    }
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("wormhole.in"));
		String line = file.readLine();
		int numOfHoles = Integer.parseInt(line);
		int[] nextToHole = new int[numOfHoles];
		int[] xArray = new int[numOfHoles];
		int[] yArray = new int[numOfHoles];
		for (int i=0; i<numOfHoles; i++) {
			line = file.readLine();
			StringTokenizer st = new StringTokenizer(line);
			xArray[i] = Integer.parseInt(st.nextToken());
			yArray[i] = Integer.parseInt(st.nextToken());
		}
		file.close();
		Integer[] arrayOfHoles = new Integer[numOfHoles];
		for (int k=0; k< numOfHoles; k++) {
			arrayOfHoles[k]=k;
		}
		for (int l=0; l< numOfHoles; l++) {
			nextToHole[l]=-1;
			for (int m=0; m<numOfHoles;m++) {
				if (isNextTo(xArray[l],yArray[l],xArray[m],yArray[m],xArray,yArray)) {
					nextToHole[l]=m;
				}
			}
		}
		int counter=0;
		int current=0;
		Set<Integer> set = new LinkedHashSet<Integer>(Arrays.asList(arrayOfHoles));

	        ArrayList<List<List<Integer>>> results = new ArrayList<List<List<Integer>>>();
	        compute(set, new ArrayList<List<Integer>>(), results);
	        for (List<List<Integer>> result : results){
	        	for (int k=0; k<numOfHoles;k++) {
	        		current=k;
	        		for (int i =0; i< numOfHoles; i++) {
	        			if (current == -1) {
	        				continue;
	        			}
	        			current = nextToHole[current];
	        			for (int j=0; j<result.size();j++) {
	        				if (result.get(j).contains(current)) {
	        					if (result.get(j).get(0)==current) {
	        						current = result.get(j).get(1);
	        						break;
	        					} else {
	        						current = result.get(j).get(0);
	        						break;
	        					}
	        				}
	        			}
	        		
	        		}
	        		if (current != -1) {
	        			counter++;
	        			break;
	        		}
	        	}
	        }
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("wormhole.out")));
		outputFile.println(counter);
		outputFile.close();
	}
}
