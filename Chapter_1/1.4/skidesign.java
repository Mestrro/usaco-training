import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/*
ID: marko.m3
LANG: JAVA
TASK: skidesign
*/
class skidesign {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("skidesign.in"));
		String line = file.readLine();
		int num = Integer.parseInt(line);
		int[] peaks = new int[num];
		int[] change = new int[num];
		int upper=0;
		int lower=0;
		int sum=0;
		for (int i=0; i<num; i++) {
			line = file.readLine();
			peaks[i] = Integer.parseInt(line);
			change[i] = 0;
		}
		Arrays.parallelSort(peaks);
		file.close();
		while (peaks[num-1]-peaks[0]>17) {
			lower=0;
			upper=0;
			for (int m=0; m< num;m++) {
				if (peaks[m]==peaks[0]) {
					lower = lower + (change[m]+1)*(change[m]+1);
				} else {
					lower = lower + change[m]*change[m];
				}
				if (peaks[m]==peaks[num-1]) {
					upper = upper + (change[m]+1)*(change[m]+1);
				} else {
					upper = upper + change[m]*change[m];
				}
			}
			if (upper > lower) {
				int lowPeak = peaks[0];
				for (int j=0; j<num; j++) {
					if (peaks[j]==lowPeak) {
						peaks[j]++;
						change[j]++;
					} else {
						break;
					}
				}
				} else {
					int highPeak = peaks[num-1];
					for (int k=num-1; k>0; k--) {
						if (peaks[k]==highPeak) {
							peaks[k]--;
							change[k]++;
						} else {
							break;
						}
					}
				}
		}

		for(int l =0; l< num; l++) {
			sum = sum + change[l]*change[l];
		}
		System.out.println(sum);
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("skidesign.out")));
		outputFile.println(sum);
		outputFile.close();
	}
}
