/*
ID: marko.m3
LANG: JAVA
TASK: barn1
*/
import java.io.*;
import java.util.*;
class barn1 {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("barn1.in"));
		String line = file.readLine();
		StringTokenizer st = new StringTokenizer(line);
		int maxBoards = Integer.parseInt(st.nextToken());
		int cows = Integer.parseInt(st.nextToken());
		int occupied[] = new int[cows];
		int differences[] = new int[cows-1];

		LinkedHashMap<Integer, String> stallsAndBoards = new LinkedHashMap<Integer, String>();
		for (int i = 0; i < cows; i++) {
			line = file.readLine();
			occupied[i] = Integer.parseInt(line);
		}
		Arrays.parallelSort(occupied);
		file.close();
		for (int j = 0; j < cows-1; j++) {
			differences[j]=occupied[j+1]-occupied[j]-1;
		}
		for (int k=0; k < cows-1; k++) {
			if (stallsAndBoards.containsKey(differences[k])) {
				stallsAndBoards.put(differences[k], stallsAndBoards.get(differences[k])+" "+Integer.toString(occupied[k]));	
			} else {
				stallsAndBoards.put(differences[k], Integer.toString(occupied[k]));	
			}
		}
		String currentStall="a";
		int currentBoards = occupied.length;
		int currentCovered = occupied.length;
		Map<Integer, String> sortedMap = new TreeMap<Integer, String>(stallsAndBoards);
		for (Map.Entry<Integer,String> entry : sortedMap.entrySet()) {
			if (currentBoards <= maxBoards) {
				break;
			}
			StringTokenizer token = new StringTokenizer(entry.getValue());
			while (token.hasMoreTokens()) {
				currentCovered = currentCovered + entry.getKey();
				currentBoards--;
				if (currentBoards <= maxBoards) {
					break;
				}
				currentStall = token.nextToken();
			}
		}
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("barn1.out")));
		outputFile.println(currentCovered);
		outputFile.close();
	}
}
