/*
ID: marko.m3
LANG: JAVA
TASK: milk
*/
import java.io.*;
import java.util.*;

class milk {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("milk.in"));
		String line = file.readLine();
		
		StringTokenizer token = new StringTokenizer(line);
		int neededAmount = Integer.parseInt(token.nextToken());
		LinkedHashMap<Integer, Integer> priceAndAmount = new LinkedHashMap<Integer, Integer>();
		int price;
		int amount;
		int counter=0;
		int minimalPay = 0;
		line=file.readLine();
		counter ++;
		while (line!=null) {
			 
			 StringTokenizer st = new StringTokenizer(line);
			 price = Integer.parseInt(st.nextToken());
			 amount = Integer.parseInt(st.nextToken());
			 if (priceAndAmount.containsKey(price)) {
				 priceAndAmount.put(price, priceAndAmount.get(price)+amount);
			 }else {
			 priceAndAmount.put(price, amount);
			 }
			 line=file.readLine();
			 counter++;
		}
		System.out.println(counter);
		file.close();
		int sum=0;
		Map<Integer, Integer> sortedMap = new TreeMap<Integer, Integer>(priceAndAmount);
		for (Map.Entry<Integer,Integer> entry : sortedMap.entrySet()) {
			for (int i=0; i<entry.getValue();i++) {
				neededAmount--;
				minimalPay = minimalPay + entry.getKey();
				if (neededAmount ==0) {
					break;
				}
			}
			sum = sum+entry.getValue();
			if (neededAmount ==0) {
				break;
			}
		}
		PrintWriter outputFile = new PrintWriter(new BufferedWriter(new FileWriter("milk.out")));
		outputFile.println(minimalPay);
		outputFile.close();
    } 
}

