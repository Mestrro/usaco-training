/*
ID: marko.m3
LANG: JAVA
TASK: combo
*/
import java.io.*;
import java.util.*;
class combo {
	static String[] generateLock(String line, int numOfDials) {
		String[] lock = new String[3];
		StringTokenizer st = new StringTokenizer(line);
		int i = 0;
		while (st.hasMoreTokens()) {
			lock[i]="";
			int token=Integer.parseInt(st.nextToken());
			for (int j=-2;j<3;j++) {
				
				if (token+j > numOfDials) {
					if (token+j+numOfDials<=0) {
						continue;
					}
					if (token+j-numOfDials > numOfDials) {
						continue;
					}
					if (!(lock[i].contains(" "+Integer.toString(token+j-numOfDials)+" "))) {
						lock[i] =lock[i]+" "+Integer.toString(token+j-numOfDials)+" ";
						continue;
					}
				} else
				if (token+j > 0 && token+j < numOfDials) {
					System.out.println(token+j);
					if (!(lock[i].contains(" "+Integer.toString(token+j)+" "))) {
						lock[i] =lock[i]+" "+Integer.toString(token+j)+" ";
						continue;
					}
					
				} else if (token+j+numOfDials<=0) {
					continue;
				}else if (token+j<=0 && numOfDials+token+j>=0) {
					if (!(lock[i].contains(" "+Integer.toString(numOfDials+token+j)+" "))) {
					 {
						lock[i] =lock[i]+" "+Integer.toString(numOfDials+token+j)+" ";
					}
				}
				} else if(token+j==numOfDials) {
					if (!(lock[i].contains(" "+Integer.toString(token+j)+" "))) {
						 {
							lock[i] =lock[i]+" "+Integer.toString(token+j)+" ";
						}
				}
				}
			}
			System.out.println(lock[i]);
			i++;
		}
		return lock;
	}
	
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("combo.in"));
		String line = file.readLine();
		int numOfDials = Integer.parseInt(line);
		line =file.readLine();
		String[] farmerLock = generateLock(line,numOfDials);
		line=file.readLine();
		String[] masterLock = generateLock(line,numOfDials);
		file.close();
		int numOfCombo = 1;
		int helperNumOfCombo = 1;
		int takeAway = 1;
		int[] helperNum = new int[3];
		for (int i = 0; i<3; i++) {
			StringTokenizer first = new StringTokenizer(farmerLock[i]);
			while (first.hasMoreTokens()) {
				String firstToken=first.nextToken();
				StringTokenizer second = new StringTokenizer(masterLock[i]);
				while (second.hasMoreTokens()) {
					if (firstToken.equals(second.nextToken())){
						helperNum[i] = helperNum[i]+1;
					}
				}
			}
			takeAway = helperNum[i] * takeAway;
		}
		int[] sumNum = new int[6];
		for (int k = 0; k<3; k++) {
			StringTokenizer forSum = new StringTokenizer(farmerLock[k]);
			while (forSum.hasMoreTokens()) {
				forSum.nextToken();
				sumNum[k]=sumNum[k]+1;
			}
			StringTokenizer forSum2 = new StringTokenizer(farmerLock[k]);
			while (forSum2.hasMoreTokens()) {
				forSum2.nextToken();
				sumNum[k+3]=sumNum[k+3]+1;
			}
		}
		for (int l=0; l<3;l++) {
			System.out.println(sumNum[l]);
			numOfCombo = numOfCombo * sumNum[l];
			helperNumOfCombo = helperNumOfCombo *sumNum[l+3];
		}
		numOfCombo = numOfCombo-takeAway+helperNumOfCombo;
		PrintWriter outputTransform = new PrintWriter(new BufferedWriter(new FileWriter("combo.out")));
		outputTransform.println(numOfCombo);
		outputTransform.close();
		}	
	}
