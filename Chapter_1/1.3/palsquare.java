/*
ID: marko.m3
LANG: JAVA
TASK: palsquare
*/
import java.io.*;
import java.util.*;
class palsquare {
	static String putInBase(int startingNumber, int base) {
		String numInBase = "";
		String digit = "";
		while (true) {
			if (startingNumber%base<10) {
				digit = Integer.toString(startingNumber%base);
			} else if (startingNumber % base ==10) {
				digit = "A";
			} else if (startingNumber % base ==11) {
				digit = "B";
			}
			 else if (startingNumber % base ==12) {
				digit = "C";
			}
			else if (startingNumber % base ==13) {
				digit = "D"	;
			}
			else if (startingNumber % base ==14) {
				digit = "E";
			}
			else if (startingNumber % base ==15) {
				digit = "F";
			}
			else if (startingNumber % base ==16) {
				digit = "G";
			}
			else if (startingNumber % base ==17) {
				digit = "H";
			}
			else if (startingNumber % base ==18) {
				digit = "I";
			}else if (startingNumber % base ==19) {
				digit = "J";
			}
			numInBase = digit + numInBase;	
			startingNumber = startingNumber/base;

			if (startingNumber/base ==0 && startingNumber%base ==0) {
				break;
			}
		}
		return numInBase;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("palsquare.in"));
		String line = file.readLine();
		int squared =0;
		int help = 0;
		int base = Integer.parseInt(line);
		String numInBase = "";
		String digit = "";
		StringBuilder reverseNumInBase = new StringBuilder();
		String[] numbersInBase = {};
		int g=0;
		for (int i=1; i<=300; i++) {
			squared = i * i;
			help = squared;
			numInBase = putInBase(help, base);
			reverseNumInBase.append(numInBase);
			reverseNumInBase.reverse();
			if (numInBase.equals(reverseNumInBase.toString())) {
				String[] tempNumInBase = new String[g+1];
				for (int q=0; q<g; q++) {
					tempNumInBase[q] = numbersInBase[q];
				}
				tempNumInBase[g] =putInBase(i, base)+" "+numInBase;
				numbersInBase=tempNumInBase;
				g++;
			}
			numInBase ="";
			reverseNumInBase.delete(0, reverseNumInBase.length());
		}
		PrintWriter outputPal = new PrintWriter(new BufferedWriter(new FileWriter("palsquare.out")));
		for (int l=0; l< numbersInBase.length; l++) {
			outputPal.println(numbersInBase[l]);
		}
		outputPal.close();
	}
}
