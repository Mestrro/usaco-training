/*
ID: marko.m3
LANG: JAVA
TASK: dualpal
*/
import java.io.*;
import java.util.*;
class dualpal {
	static String putInBase(int startingNumber, int base) {
		String numInBase = "";
		String digit = "";

		while (true) {
			digit = Integer.toString(startingNumber%base);
			numInBase = digit + numInBase;	
			startingNumber = startingNumber/base;

			if (startingNumber/base ==0 && startingNumber%base ==0) {
				break;
			}
		}
		return numInBase;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("dualpal.in"));
		String line = file.readLine();
		file.close();
		StringTokenizer st = new StringTokenizer(line);
		int requestingNum = Integer.parseInt(st.nextToken());
		int startingNum = Integer.parseInt(st.nextToken());
		
		int counter;
		int palCounter =0;
		String numInBase;
		int i=startingNum;
		StringBuilder reverseNumInBase = new StringBuilder();
		PrintWriter outputPal = new PrintWriter(new BufferedWriter(new FileWriter("dualpal.out")));
		while(true) {
			i++;
			counter = 0;
			for (int j=2; j<=10;j++) {
				numInBase = putInBase(i, j);
				reverseNumInBase.delete(0, reverseNumInBase.length());
				reverseNumInBase.append(numInBase);
				reverseNumInBase.reverse();
				if (numInBase.equals(reverseNumInBase.toString())) {
					counter++;
				}
				if (counter == 2) {
					outputPal.println(i);
					palCounter++;
					break;
				}

			}

			if (palCounter ==requestingNum) {
				break;
			}

		}

		outputPal.close();
	}
}
