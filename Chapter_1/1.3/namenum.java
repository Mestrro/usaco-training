/*
ID: marko.m3
LANG: JAVA
TASK: namenum
*/
import java.io.*;
import java.util.*;
class namenum {
	static String cowNumbers(String kajgod) {
		String cowNumber;
		String cowLetter = kajgod;
		if (cowLetter.equals("A") || cowLetter.equals("B") || cowLetter.equals("C")) {
			cowNumber ="2";
		} else
		if (cowLetter.equals("D") || cowLetter.equals("E") || cowLetter.equals("F")) {
			cowNumber ="3";
		} else
		if (cowLetter.equals("G") || cowLetter.equals("H") || cowLetter.equals("I")) {
			cowNumber ="4";
		} else
		if (cowLetter.equals("J") || cowLetter.equals("K") || cowLetter.equals("L")) {
			cowNumber ="5";
		} else
		if (cowLetter.equals("M") || cowLetter.equals("N") || cowLetter.equals("O")) {
			cowNumber ="6";
		} else
		if (cowLetter.equals("P") || cowLetter.equals("R") || cowLetter.equals("S")) {
			cowNumber ="7";
		} else
		if (cowLetter.equals("T") || cowLetter.equals("U") || cowLetter.equals("V")) {
			cowNumber ="8";
		} else
		if (cowLetter.equals("W") || cowLetter.equals("X")|| cowLetter.equals("Y")) {
			cowNumber ="9";
		} else {
			cowNumber = "";
		}
		
		return cowNumber;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("namenum.in"));
		String line = file.readLine();
		file.close();
		BufferedReader file2 = new BufferedReader(new FileReader("dict.txt"));
		String line2 = file2.readLine();
		String[] dictionaryOfCows = {};
		int i = 0;
		while (line2 !=null) {
			if (line2.length()==line.length()) {
				String[] tempDict = new String[i+1];
				for (int a = 0; a < i; a++) {
		            tempDict[a] = dictionaryOfCows[a]; 
				}
		        tempDict[i] = line2;
				dictionaryOfCows = tempDict;
				i++;	
			}
			line2 = file2.readLine();
		}
		file2.close();
		String[] passableNames = {};
		String cowName="";
		int w = 0;
		for (int j=0; j<dictionaryOfCows.length; j++) {
			for (int k=0; k<line.length(); k++) {
				cowName = cowName+cowNumbers(Character.toString(dictionaryOfCows[j].charAt(k)));
			}
			if (cowName.equals(line)) {
				String[] tempName = new String[w+1];
				for (int a = 0; a < w; a++) {
		            tempName[a] = passableNames[a]; 
				}
		        tempName[w] = dictionaryOfCows[j];
				passableNames = tempName;
				w++;
			}
			cowName = "";
		}

		PrintWriter outputCow = new PrintWriter(new BufferedWriter(new FileWriter("namenum.out")));
		if (passableNames.length == 0) {
			outputCow.println("NONE");
		} else {
			for (int l=0; l< passableNames.length; l++) {
				outputCow.println(passableNames[l]);
			}
		}
		outputCow.close();
		}
	}
