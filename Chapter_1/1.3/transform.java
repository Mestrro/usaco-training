/*
ID: marko.m3
LANG: JAVA
TASK: transform
*/
import java.io.*;
import java.util.*;

class transform {
	static String[][] ninetyTransform(String[][] startingMatrix, int dimension) {
		String[][] newMatrix = new String[dimension][dimension];
		for (int i=0; i < dimension; i++) {
			for (int j=0; j < dimension; j++) {
				newMatrix[j][dimension-1-i]=startingMatrix[i][j];
			}
		}
		return newMatrix;	
	}
	static String[][] verticalTransform(String[][] startingMatrix, int dimension){
		String[][] newMatrix = new String[dimension][dimension];
		for (int i=0; i < dimension; i++) {
			for (int j=0; j < dimension; j++) {
				newMatrix[i][dimension-1-j]=startingMatrix[i][j];
			}
		}
		return newMatrix;
	}
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("transform.in"));
		String line = file.readLine();
		int type = 0;
		int dimension = Integer.parseInt(line);
		String[][] beforeTransform = new String[dimension][dimension];
		String[][] afterTransform = new String[dimension][dimension];
		String[][] transformNinety = new String[dimension][dimension];
		String[][] transformOneEighty = new String[dimension][dimension];
		String[][] transformTwoSeventy = new String[dimension][dimension];
		String[][] transformV = new String[dimension][dimension];
		String[][] transformVNinety = new String[dimension][dimension];
		String[][] transformVOneEighty = new String[dimension][dimension];
		String[][] transformVTwoSeventy = new String[dimension][dimension];
		for (int i = 0; i < 2*dimension; i++) {
			line = file.readLine();
			if (i<dimension) {
				for (int j = 0; j < dimension; j++) {
					beforeTransform[i][j] = Character.toString(line.charAt(j));
				} 
			}
			else {
					for (int j = 0; j < dimension; j++) {
						afterTransform[i-dimension][j] = Character.toString(line.charAt(j));
				}
			}
		}
		for (int k = 0; k < dimension; k++) {
			for (int l = 0; l < dimension; l++) {
				
			}
		}
		file.close();

		transformNinety = ninetyTransform(beforeTransform, dimension);
		System.out.println("Transform 90");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformNinety[i][j]);
			}
			System.out.println("");
		}
		transformOneEighty = ninetyTransform(transformNinety, dimension);
		System.out.println("Transform 180");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformOneEighty[i][j]);
			}
			System.out.println("");
		}
		transformTwoSeventy = ninetyTransform(transformOneEighty, dimension);
		System.out.println("Transform 270");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformTwoSeventy[i][j]);
			}
			System.out.println("");
		}
		transformV = verticalTransform(beforeTransform, dimension);
		System.out.println("Transform V");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformV[i][j]);
			}
			System.out.println("");
		}
		transformVNinety = ninetyTransform(transformV, dimension);
		System.out.println("Transform V90");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformVNinety[i][j]);
			}
			System.out.println("");
		}
		transformVOneEighty = ninetyTransform(transformVNinety, dimension);
		System.out.println("Transform V180");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformVOneEighty[i][j]);
			}
			System.out.println("");
		}
		transformVTwoSeventy = ninetyTransform(transformVOneEighty, dimension);
		System.out.println("Transform V270");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(transformVTwoSeventy[i][j]);
			}
			System.out.println("");
		}

		System.out.println("After Transform");
		for (int i= 0; i< dimension; i++) {

			for (int j= 0; j< dimension; j++) {
				System.out.print(afterTransform[i][j]);
			}
			System.out.println("");
		}

		if (Arrays.deepEquals(afterTransform, transformNinety)){
			type = 1;
		} else if (Arrays.deepEquals(afterTransform, transformOneEighty)) {
			type = 2;
		} else if (Arrays.deepEquals(afterTransform, transformTwoSeventy)) {
			type = 3;
		} else if (Arrays.deepEquals(afterTransform, transformV)) {
			type = 4;
		} else if (Arrays.deepEquals(afterTransform, transformVNinety) || Arrays.deepEquals(afterTransform, transformVOneEighty) || Arrays.deepEquals(afterTransform, transformVTwoSeventy)) {
			type = 5;
		} else if (Arrays.deepEquals(afterTransform, beforeTransform)){
			type = 6;
		} else {
			type = 7;
		}
		PrintWriter outputTransform = new PrintWriter(new BufferedWriter(new FileWriter("transform.out")));
		outputTransform.println(type);
		outputTransform.close();
	}
}
