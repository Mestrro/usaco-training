/*
ID: marko.m3
LANG: JAVA
TASK: milk2
*/
import java.io.*;
import java.util.*;
class milk2 {
	public static void main (String [] args) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader("milk2.in"));
		String line = file.readLine();
		int numOfFarmers = Integer.parseInt(line);
		int[] farmerWorkStart = new int[numOfFarmers];
		int[] newFarmerWorkStart = new int[numOfFarmers];
		int[] farmerWorkEnd = new int[numOfFarmers];
		int[] newFarmerWorkEnd = new int[numOfFarmers];
		int[] oldFarmerWorkStart = new int[numOfFarmers];
		int[] oldFarmerWorkEnd = new int[numOfFarmers];
		oldFarmerWorkStart = farmerWorkStart;
		oldFarmerWorkEnd = farmerWorkEnd;
		for (int i = 0; i < numOfFarmers; i++) {
			line = file.readLine();
			StringTokenizer st = new StringTokenizer(line);
			farmerWorkStart[i] = Integer.parseInt(st.nextToken()); 
			farmerWorkEnd[i] = Integer.parseInt(st.nextToken());
		}
		int biggestNonWorkingTime = 0;
		int biggestWorkingTime = 0;
		while (newFarmerWorkStart != oldFarmerWorkStart && newFarmerWorkEnd != oldFarmerWorkEnd){
			newFarmerWorkStart = oldFarmerWorkStart;
			newFarmerWorkEnd = oldFarmerWorkEnd;
			for (int i = 0; i < oldFarmerWorkEnd.length; i++) {
				for (int j = 0; j < oldFarmerWorkStart.length; j++) {
					if (i==j) {
						j++;
						continue;
					}
					if (i==oldFarmerWorkEnd.length) {
						break;
					}
					if (oldFarmerWorkStart[j]<= oldFarmerWorkEnd[i]) {
						if (oldFarmerWorkEnd[j]>=oldFarmerWorkEnd[i]) {
							oldFarmerWorkEnd[i] = oldFarmerWorkEnd[j];
							int[] temporaryArrayS =  new int[oldFarmerWorkStart.length-1];
							int[] temporaryArrayE =  new int[oldFarmerWorkEnd.length-1];
							if (oldFarmerWorkStart[j] >= oldFarmerWorkStart[i]) {
								System.arraycopy(oldFarmerWorkStart, 0, temporaryArrayS, 0, j);
								if (j < oldFarmerWorkStart.length-1) {
									System.arraycopy(oldFarmerWorkStart, j+1, temporaryArrayS, j, oldFarmerWorkStart.length-j-1);
								} 
							}
							else {
								oldFarmerWorkStart[i] = oldFarmerWorkStart[j];
								System.arraycopy(oldFarmerWorkStart, 0, temporaryArrayS, 0, j);
								if (j < oldFarmerWorkStart.length-1) {
									System.arraycopy(oldFarmerWorkStart, j+1, temporaryArrayS, j, oldFarmerWorkStart.length-j-1);
								}
							}
							
							System.arraycopy(oldFarmerWorkEnd, 0, temporaryArrayE, 0, j);
							if (j < oldFarmerWorkEnd.length-1) {
								System.arraycopy(oldFarmerWorkEnd, j+1, temporaryArrayE,j, oldFarmerWorkStart.length-j-1);
	
							}
							oldFarmerWorkStart = temporaryArrayS;
							oldFarmerWorkEnd = temporaryArrayE;
							if (oldFarmerWorkStart.length == 1) {
								break;
							}
							j--;
							if (i==oldFarmerWorkEnd.length-1) {
								i--;
							}
						}
					}	
				
				}
			}

		}
		Arrays.parallelSort(newFarmerWorkStart);
		Arrays.parallelSort(newFarmerWorkEnd);
		for (int l = 0; l < newFarmerWorkStart.length; l++) {
			if (newFarmerWorkEnd[l] - newFarmerWorkStart[l] > biggestWorkingTime) {
				biggestWorkingTime = newFarmerWorkEnd[l] - newFarmerWorkStart[l];
			}
			if (l+1 < newFarmerWorkStart.length) {
				if (newFarmerWorkStart[l+1] - newFarmerWorkEnd[l] > biggestNonWorkingTime) {
					biggestNonWorkingTime = newFarmerWorkStart[l+1] - newFarmerWorkEnd[l];
				}
			}
		}
		PrintWriter outputMilk = new PrintWriter(new BufferedWriter(new FileWriter("milk2.out")));
		outputMilk.println(biggestWorkingTime+" "+biggestNonWorkingTime);
		outputMilk.close();
	}
}
